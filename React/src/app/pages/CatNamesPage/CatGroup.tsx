import * as React from "react";
import { ICatOwnerGroup } from "./CatNameInterfaces";

interface IProps {
    data: ICatOwnerGroup
}

export class CatGroup extends React.Component<IProps, {}> {

    render() {
        return <ul className="list-group">
            <li className="list-group-item active">{this.props.data.ownerGender}</li>
            {this.props.data.cats.map(c => <li key={c.name} className="list-group-item">{c.name}</li>)}
        </ul>
    }
}
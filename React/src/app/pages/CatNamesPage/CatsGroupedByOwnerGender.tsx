import * as React from "react"
import { CatGroup } from "./CatGroup"
import { ICatOwnerGroup } from "./CatNameInterfaces";

interface IProps {
    data: ICatOwnerGroup[]
}

export class CatsGroupedByOwnerGender extends React.Component<IProps, {}> {
    render() : JSX.Element { 
        return <>{ this.props.data.map(x => <CatGroup data = { x } />) }</>
    }
}
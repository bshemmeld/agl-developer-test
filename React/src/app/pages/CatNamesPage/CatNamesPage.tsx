import * as React from "react";
import axios, { AxiosResponse, AxiosRequestConfig, AxiosPromise } from "axios";
import { CatsGroupedByOwnerGender } from "./CatsGroupedByOwnerGender"
import { IOwner, ICatOwnerGroup, IPet } from "./CatNameInterfaces"
import * as _ from "lodash";

interface IProps {

}

interface IState {
    CatsGroupedByOwnerGender: any[]
}

export class CatNamesPage extends React.Component<IProps, IState> {

    constructor(props: IProps) {
        super(props);

        this.state = { CatsGroupedByOwnerGender: [] };
    }

    private loadPetData = (url: string): void => {
        axios.get<any>(url)
            .then((response: AxiosResponse<IOwner[]>) => {

                var groups = _.chain(response.data).groupBy("gender")
                    .map((owner, gender): ICatOwnerGroup => {
                        return {
                            ownerGender: gender,
                            cats: _.chain(owner)
                                .filter(o => o.pets != null)
                                .map(o => o.pets)
                                .reduce((a, b) => a.concat(b))
                                .filter(p => p.type == "Cat")
                                .value()
                        }
                    })
                    .value();
                console.log(groups);

                this.setState({ CatsGroupedByOwnerGender: groups })

            });
    }

    componentWillMount() {
        this.loadPetData("http://agl-developer-test.azurewebsites.net/people.json");
    }

    render() {
        return <>
            <h1>Cat Names Grouped By Owner Gender</h1>
            <CatsGroupedByOwnerGender data={this.state.CatsGroupedByOwnerGender} />
        </>
    }
}
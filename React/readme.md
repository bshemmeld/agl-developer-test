##Programming challenge
A json web service has been set up at the url: 
[http://agl-developer-test.azurewebsites.net/people.json](http://agl-developer-test.azurewebsites.net/people.json)

You need to write some code to consume the json and output a list of all the cats in alphabetical order under a heading of the gender of their owner.
You can write it in any language you like. You can use any libraries/frameworks/SDKs you choose.

Example:

#####Male
- Angel
- Molly
- Tigger

#####Female
- Gizmo
- Jasper

Notes:

Submissions will only be accepted via github or bitbucket. Use industry best practices. Use the code to showcase your skill.

##Details about my submission
I have built a site using ASP.NET MVC, and demonstrated 2 methods of retrieving and displaying the required data.

In both cases a HTTP GET request is performed.  The data is then grouped, filtered and shaped according to the requirements and displayed in the web browser.

1. Server side (using C# and ASP.NET)
2. Client-side (using React.js and TypeScript)

﻿using System;
using System.Collections.Generic;
using System.Linq;
using AGLDeveloperTest.Models;
using AGLDeveloperTest.ViewModels;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    [TestClass]
    public class ViewModelTests
    {
        private List<Owner> _sampleOwners;

        [TestInitialize]
        public void Setup()
        {
            _sampleOwners = new List<Owner>
            {
                new Owner
                {
                    Age = 38,
                    Name = "Ben",
                    Gender = "Male",
                    Pets = new List<Pet>
                    {
                        new Pet {Name = "Darcy", Type = "Dog"},
                        new Pet {Name = "Leila", Type = "Cat"},
                        new Pet {Name = "Stella", Type = "Cat"}
                    }
                },
                new Owner
                {
                    Age = 41,
                    Name = "Julia",
                    Gender = "Female",
                    Pets = new List<Pet>
                    {
                        new Pet {Name = "Fishy", Type = "Fish"},
                        new Pet {Name = "Elsa", Type = "Cat"},
                    }
                },
                new Owner
                {
                    Age = 11,
                    Name = "Alexander",
                    Gender = "Male",
                    Pets = new List<Pet>
                    {
                        new Pet {Name = "Garfield", Type = "Cat"},
                    }
                },
                new Owner
                {
                    Age = 74,
                    Name = "Graeme",
                    Gender = "Male",
                    Pets = null
                }
            };
        }

        [TestMethod]
        public void GetCatGroupingViewModel()
        {
            //Act
            var viewModel = new CatGroupingViewModel(_sampleOwners);

            CollectionAssert.AreEqual(new [] {"Male", "Female"}, viewModel.OwnerGenders.Select(x => x.Gender).ToArray());
            CollectionAssert.AreEqual(new [] { "Leila","Stella","Garfield" }, viewModel.OwnerGenders.Where(x => x.Gender == "Male").SelectMany(x => x.Cats.Select(c => c.Name)).ToArray());
            CollectionAssert.AreEqual(new[] { "Elsa" }, viewModel.OwnerGenders.Where(x => x.Gender == "Female").SelectMany(x => x.Cats.Select(c => c.Name)).ToArray());
        }

        [TestMethod]
        public void GetCatGroupingViewModel_OnlyMaleOwners()
        {
            //Arrange
            //Remove all but Male gender
            _sampleOwners.Where(x => x.Gender != "Male").ToList().ForEach(x => _sampleOwners.Remove(x));

            //Act
            var viewModel = new CatGroupingViewModel(_sampleOwners);

            CollectionAssert.AreEqual(new[] { "Male" }, viewModel.OwnerGenders.Select(x => x.Gender).ToArray());
            CollectionAssert.AreEqual(new[] { "Leila", "Stella", "Garfield" }, viewModel.OwnerGenders.Where(x => x.Gender == "Male").SelectMany(x => x.Cats.Select(c => c.Name)).ToArray());
        }

    }
}

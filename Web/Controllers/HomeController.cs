﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AGLDeveloperTest.Models;
using AGLDeveloperTest.Services;
using AGLDeveloperTest.ViewModels;

namespace AGLDeveloperTest.Controllers
{
    public class HomeController : Controller
    {
        private const string PET_DATA_URL = "http://agl-developer-test.azurewebsites.net/people.json";

        private readonly IPetService _petService;

        public HomeController(IPetService petService)
        {
            _petService = petService;
        }

        public ActionResult Index()
        {
            ViewBag.ActiveMenuItem = MenuItem.AspNet_Mvc;
            List<Models.Owner> owners = null;
            try
            {
                owners = _petService.LoadPetOwners(new Uri(PET_DATA_URL));
            }
            catch (ApplicationException ex)
            {
                ViewBag.ErrorMessage = ex.Message;
            }

            var viewModel = new CatGroupingViewModel(owners);
            return View(viewModel);
        }

        public ActionResult React()
        {
            ViewBag.ActiveMenuItem = MenuItem.ReactJS;
            return View();
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AGLDeveloperTest.Models;

namespace AGLDeveloperTest.Services
{
    public interface IPetService
    {
        List<Owner> LoadPetOwners(Uri uri);
    }
}

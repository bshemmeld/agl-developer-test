﻿using System;
using System.Collections.Generic;
using System.Net;
using AGLDeveloperTest.Models;
using RestSharp;

namespace AGLDeveloperTest.Services
{
    public class PetService : IPetService
    {
        public List<Owner> LoadPetOwners(Uri uri)
        {
            var client = new RestClient($"{uri.Scheme}://{uri.Host}");
            var request = new RestRequest(uri.PathAndQuery);
            var response = client.Execute<List<Owner>>(request);
            if (response.StatusCode != HttpStatusCode.OK)
            {
                throw new ApplicationException($"Request to get data from {uri} resulted in response {response.StatusDescription}");
            }
            return response.Data;
        }
    }
}
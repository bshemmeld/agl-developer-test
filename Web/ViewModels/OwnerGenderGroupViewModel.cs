﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AGLDeveloperTest.ViewModels
{
    public class OwnerGenderGroupViewModel
    {
        public OwnerGenderGroupViewModel()
        {
            Cats = new List<CatViewModel>();
        }

        public string Gender { get; set; }
        public List<CatViewModel> Cats { get; set; }
    }
}
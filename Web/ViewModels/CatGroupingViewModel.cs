﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AGLDeveloperTest.Models;

namespace AGLDeveloperTest.ViewModels
{
    public class CatGroupingViewModel
    {
        public CatGroupingViewModel()
        {
            OwnerGenders = new List<OwnerGenderGroupViewModel>();
        }

        public CatGroupingViewModel(List<Owner> owners) : this()
        {
            if (owners == null)
                return;

            var groupedByGender = owners.GroupBy(x => x.Gender);

            foreach (var group in groupedByGender)
            {
                OwnerGenders.Add(new OwnerGenderGroupViewModel
                {
                    Gender = group.Key,
                    Cats = group.Where(x => x.Pets != null)
                        .SelectMany(x => x.Pets.Where(p => p.Type.ToLower() == "cat").Select(p => new CatViewModel { Name = p.Name }))
                        .ToList()
                });
            }
        }

        public List<OwnerGenderGroupViewModel> OwnerGenders { get; set; }
    }
}